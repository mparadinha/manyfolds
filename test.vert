#version 330 core

uniform mat4 m, p, v;
uniform vec3 color;

uniform float param;

layout (location = 0) in vec3 pos;

out vec3 pass_color;

void main() {
    pass_color = color;
    vec3 le_pos = vec3(pos.x, 0.2 * sin(pos.x + pos.z - 2 * param), pos.z);
    gl_Position = p * v * m * vec4(le_pos, 1);
}
