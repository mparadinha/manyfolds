#version 330 core

in vec3 pass_color;
out vec4 FragColor;

void main() {
    FragColor = vec4(pass_color, 1);
}
