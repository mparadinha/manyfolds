#include "mesh.h"
#include "load_gl.h"
#include "gl_math.h"

void load_into_gpu(Mesh* objmesh) {
    glGenVertexArrays(1, &(objmesh->vao));
    glBindVertexArray(objmesh->vao);

    glGenBuffers(1, &(objmesh->vbo));
    glBindBuffer(GL_ARRAY_BUFFER, objmesh->vbo);
    glBufferData(GL_ARRAY_BUFFER, objmesh->verts.size() * sizeof(vec3), objmesh->verts.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void*) 0);
    glEnableVertexAttribArray(0);

    uint32_t normals_vbo;
    glGenBuffers(1, &normals_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, normals_vbo);
    glBufferData(GL_ARRAY_BUFFER, objmesh->normals.size() * sizeof(vec3), objmesh->normals.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void*) 0);
    glEnableVertexAttribArray(1);

    uint32_t texcoords_vbo;
    glGenBuffers(1, &texcoords_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, texcoords_vbo);
    glBufferData(GL_ARRAY_BUFFER, objmesh->texcoords.size() * sizeof(vec2), objmesh->texcoords.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), (void*) 0);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &(objmesh->ebo));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, objmesh->ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, objmesh->indices.size() * sizeof(uint32_t), objmesh->indices.data(), GL_STATIC_DRAW);
}
