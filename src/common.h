#pragma once

#include <vector>
#include <string>

struct KeyEvent {
    int32_t key, scancode, action, mods;
};

struct MouseEvent {
    int32_t button, action, mods;
};

struct Events {
    std::vector<KeyEvent> key_events;
    std::vector<MouseEvent> mouse_events;
};

struct ErrorList {
    std::string shader_compile;
};

//extern std::vector<std::string> debug_warnings;


/* misc functions */
void print_debug(const char* fmt, ...);
uint8_t* read_whole_file(const char* filepath);
