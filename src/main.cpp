#include <assert.h>
#include <stdint.h>
#include <stdio.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include "camera.h"
#include "gl_math.h"
#include "load_gl.h"
#include "mesh.h"
#include "text.h"

const uint32_t win_w = 1600;
const uint32_t win_h = 900;

GLFWwindow* window;

std::vector<uint32_t> utf32_str;
bool refresh_param = false;
uint32_t cursor = 0;
float scroll_offset = 0;

const char* vertex_src =
    "#version 330 core\n"
    "uniform mat4 m, p, v;\n"
    "uniform vec3 color;\n"
    "uniform float time;\n"
    "layout (location = 0) in vec3 pos;\n"
    "out vec3 pass_color;\n"
    "void main() {\n"
    "    pass_color = color;\n"
    "    vec3 le_pos = vec3(pos.x, %s, pos.z);\n"
    "    gl_Position = p * v * m * vec4(le_pos, 1);\n"
    "}\n";

const char* fragment_src =
    "#version 330 core\n"
    "in vec3 pass_color;\n"
    "out vec4 FragColor;\n"
    "void main() { FragColor = vec4(pass_color, 1); }\n";

void glfw_char_callback(GLFWwindow* window, unsigned int codepoint) {
    utf32_str.insert(utf32_str.begin() + cursor, codepoint);
    cursor++;
}

void glfw_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if(action != GLFW_RELEASE) {
        if(key == GLFW_KEY_BACKSPACE) {
            if (utf32_str.size() > 0 && cursor > 0) {
                utf32_str.erase(utf32_str.begin() + cursor - 1);
                cursor--;
            }
        }
        if(key == GLFW_KEY_DELETE && cursor < utf32_str.size()) {
            utf32_str.erase(utf32_str.begin() + cursor);
        }
    }

    if(action == GLFW_RELEASE || action == GLFW_REPEAT) {
        if(key == GLFW_KEY_ENTER) { refresh_param = true; }
        if(key == GLFW_KEY_LEFT && cursor > 0) { cursor--; }
        if(key == GLFW_KEY_RIGHT && cursor <= utf32_str.size()) { cursor++; }
    }

    if(action == GLFW_RELEASE) {
        if(key == GLFW_KEY_HOME) { cursor = 0; }
        if(key == GLFW_KEY_END) { cursor = utf32_str.size(); }
    }
}

void glfw_scroll_callback(GLFWwindow* window, double xoff, double yoff) {
    scroll_offset = yoff;
}

Mesh build_grid_mesh(uint32_t divs_x, uint32_t divs_z, float div_size) {
    Mesh mesh;
    for (uint32_t j = 0; j < divs_z; j++) {
        for (uint32_t i = 0; i < divs_x; i++) {
            mesh.verts.push_back(
                vec3({i * div_size, 0, j * div_size}) -
                (div_size / 2) * vec3({(float)divs_x, 0, (float)divs_z})
                // and to center around (0, 0, 0)
            );
        }
    }
    for (uint32_t j = 0; j < divs_z - 1; j++) {
        for (uint32_t i = 0; i < divs_x - 1; i++) {
            uint32_t start = i + j * divs_x;
            mesh.indices.push_back(start);
            mesh.indices.push_back(start + divs_x + 1);
            mesh.indices.push_back(start + 1);
            mesh.indices.push_back(start);
            mesh.indices.push_back(start + divs_x);
            mesh.indices.push_back(start + divs_x + 1);
        }
    }
    return mesh;
}

uint32_t make_quad_vao() {
    float verts[] = {0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                     1.0, 1.0, 0.0, 0.0, 1.0, 0.0};
    uint32_t indices[] = {0, 1, 2, 0, 2, 3};

    uint32_t vao, vbo, ebo;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*) 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*) 0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    return vao;
} 

const char* frame_vert_src =
    "#version 330 core\n"
    "uniform mat4 m, p, v;\n"
    "layout (location = 0) in vec3 pos;\n"
    "void main() {\n"
    "    gl_Position = p * v * m * vec4(pos, 1);\n"
    "}\n";

const char* frame_frag_src =
    "#version 330 core\n"
    "uniform vec3 color;\n"
    "out vec4 FragColor;\n"
    "void main() { FragColor = vec4(color, 0.75); }\n";

Mesh make_frame() {  
    Mesh mesh = {
        .verts = {
            { 0.7576,  0.025, -0.025},
            { 0.7576, -0.025, -0.025},
            { 0.7576, -0.025,  0.025},
            { 0.7576,  0.025,  0.025},
            {    1.0,      0,      0},
            {    0.0, -0.025,  0.025},
            {    0.0,  0.025,  0.025},
            {    0.0, -0.025, -0.025},
            {    0.0,  0.025, -0.025},
            { 0.7780,   0.06,  -0.06},
            { 0.7780,  -0.06,  -0.06},
            { 0.7780,  -0.06,   0.06},
            { 0.7780,   0.06,   0.06},
        },

        .indices = {
            10, 5, 11,
            11, 5, 12,
            12, 5, 13,
            13, 5, 10,

            6, 7, 9,   6, 9, 8, 
            8, 9, 1,   8, 1, 2,
            3, 4, 7,   3, 7, 6,
            8, 2, 3,   8, 3, 6,
            1, 9, 7,   1, 7, 4,
            2, 1, 10,  2, 10, 1,
            3, 2, 11,  3, 11, 1,
            4, 3, 12,  4, 12, 1,
            1, 4, 13,  1, 13, 1,
        },
    };
    // convert to 0 indexed
    for(auto i = 0; i < mesh.indices.size(); i++) mesh.indices[i]--;

    return mesh;
}

void draw_frame(Mesh frame_mesh, uint32_t shader_id, mat4 m) {
    mat4 x_m = m;
    mat4 y_m = m * rotation_matrix({0, 0, 1}, M_PI / 2);
    mat4 z_m = m * rotation_matrix({0, 1, 0}, -M_PI / 2);

    glBindVertexArray(frame_mesh.vao);

    glUniform3f(glGetUniformLocation(shader_id, "color"), 1, 0, 0);
    glUniformMatrix4fv(glGetUniformLocation(shader_id, "m"),
        1, GL_FALSE, (float*)&x_m);
    glDrawElements(GL_TRIANGLES, frame_mesh.indices.size(), GL_UNSIGNED_INT, NULL);

    glUniform3f(glGetUniformLocation(shader_id, "color"), 0, 1, 0);
    glUniformMatrix4fv(glGetUniformLocation(shader_id, "m"),
        1, GL_FALSE, (float*)&y_m);
    glDrawElements(GL_TRIANGLES, frame_mesh.indices.size(), GL_UNSIGNED_INT, NULL);

    glUniform3f(glGetUniformLocation(shader_id, "color"), 0, 0, 1);
    glUniformMatrix4fv(glGetUniformLocation(shader_id, "m"),
        1, GL_FALSE, (float*)&z_m);
    glDrawElements(GL_TRIANGLES, frame_mesh.indices.size(), GL_UNSIGNED_INT, NULL);
}

void draw_vector_field(Mesh frame_mesh, uint32_t shader) {}

int main() {
    assert(glfwInit());
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    window = glfwCreateWindow(win_w, win_h, "i3floatme", NULL, NULL);
    glfwMakeContextCurrent(window);
    glfwSetCharCallback(window, glfw_char_callback);
    glfwSetKeyCallback(window, glfw_key_callback);
    glfwSetScrollCallback(window, glfw_scroll_callback);
    // glfwSetMouseButtonCallback(window, glfw_mouse_callback);
    // glfwSetFramebufferSizeCallback(window, glfw_framebuffer_callback);
    // glfwSetWindowUserPointer(window, &events);
    glfwSwapInterval(1);

    load_gl();
    printf("opengl version: %s\n", glGetString(GL_VERSION));

    // need to enable this for the text rendering
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // some more state that we never change
    // glEnable(GL_CULL_FACE);
    glClearColor(0.2, 0.87, 0.82, 1);

    float fov = 60.0f, near = 0.1f, far = 1000.0f;
    float ratio = (float)win_w / (float)win_h;
    float yaw = 45, pitch = -45;
    vec3 cam_pos = {sqrt(3), 2.5, sqrt(3)};
    Camera camera;
    camera.yaw = yaw;
    camera.pitch = pitch;
    camera.projection = projection_matrix(fov, ratio, near, far);
    camera.view =
        view_matrix(cam_pos, (yaw * M_PI / 180.0f), (pitch * M_PI / 180.0f));
    camera.position = cam_pos;
    camera.locked = false;

    char new_vertex_src[1000];
    std::string start_func = "0.2 * sin(pos.x + pos.z - 2 * time) - 0.7";
    snprintf(new_vertex_src, 1000, vertex_src, start_func.c_str());
    for (auto c : start_func) {
        if (c) utf32_str.push_back((uint32_t)c);
    }
    Shader test_shader = make_shader_program_src(new_vertex_src, fragment_src);
    // Shader test_shader = make_shader_program("test", ".");

    Mesh test_quad = build_grid_mesh(25, 25, 0.5);
    load_into_gpu(&test_quad);

    mat4 test_quad_m = identity_matrix();

    Font font = create_font("Gidole-Regular.ttf", 50.0f);
    //Font font = create_font("liberation-mono.ttf", 50.0f);
    Shader text_shader = make_shader_program("text", ".");

    uint32_t quad_vao = make_quad_vao();
    Shader quad_shader = make_shader_program("button", ".");

    Shader frame_shader = make_shader_program_src(frame_vert_src, frame_frag_src);
    Mesh frame_mesh = make_frame();
    load_into_gpu(&frame_mesh);

    float dt = 0, last_time = glfwGetTime();
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        update_camera_centered(&camera, window, scroll_offset, dt);
        scroll_offset = 0.0f;
        if (refresh_param) {
            if (utf32_str.size() > 0) {
                char* str = (char*)malloc(utf32_str.size() + 1);
                for (auto i = 0; i < utf32_str.size(); i++) {
                    str[i] = (char)utf32_str[i];
                }
                str[utf32_str.size()] = '\0';

                snprintf(new_vertex_src, 1000, vertex_src, str);
                test_shader = make_shader_program_src(new_vertex_src, fragment_src);
            }
            refresh_param = false;
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(test_shader.id);
        set_camera_uniforms(camera, test_shader);
        glBindVertexArray(test_quad.vao);

        glUniform1f(glGetUniformLocation(test_shader.id, "time"), glfwGetTime());

        glUniformMatrix4fv(glGetUniformLocation(test_shader.id, "m"), 1, GL_FALSE, (float*)&test_quad_m);
        glUniform3f(glGetUniformLocation(test_shader.id, "color"), 1, 1, 1);
        glDrawElements(GL_TRIANGLES, test_quad.indices.size(), GL_UNSIGNED_INT, NULL);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glUniform3f(glGetUniformLocation(test_shader.id, "color"), 0, 0, 0);
        glDrawElements(GL_TRIANGLES, test_quad.indices.size(), GL_UNSIGNED_INT, NULL);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        // draw x frame
        glUseProgram(frame_shader.id);
        set_camera_uniforms(camera, frame_shader);
        draw_frame(frame_mesh, frame_shader.id, identity_matrix());

        // render utf32_str text to screen
        if (utf32_str.size() > 0) {
            char* str = (char*)malloc(utf32_str.size() + 1);
            for (auto i = 0; i < utf32_str.size(); i++) {
                str[i] = (char)utf32_str[i];
            }
            str[utf32_str.size()] = '\0';
            auto buffer = create_string_verts(font, str, {-1, 0.95}, 900.0f, 16.0f / 9.0f);
            glUseProgram(text_shader.id);
            set_camera_uniforms(camera, text_shader);
            glActiveTexture(GL_TEXTURE0);
            glUniform1i(glGetUniformLocation(text_shader.id, "tex"), 0);
            glBindTexture(GL_TEXTURE_2D, font.atlas.id);
            glBindVertexArray(buffer.vao);
            glDrawArrays(GL_TRIANGLES, 0, buffer.n_verts);

            free_text_buffer(buffer);
            free(str);

            // draw cursor quad
            if(cursor > utf32_str.size()) cursor = utf32_str.size();
            // each letter has 6 vertices (2 triangles) on the text buffer
            // (and each vertex has 2 floats for (x,y) pos and 2 floats for texcoord)
            vec4 bottom_left, top_right;
            // space doesn't have a quad so to calculate it's perceived size we use
            // end of the previous char and start of the next
            if(utf32_str[cursor] == 0x20) {
                if(cursor == 0) {
                    bottom_left = {-1, 0.95, 0, 0};
                }
                else {
                    bottom_left = buffer.data[(cursor - 1) * 6 + 1];
                }

                if(cursor == utf32_str.size() - 1) {
                    top_right = bottom_left;
                    top_right[0] += 0.01;
                }
                else {
                    top_right = buffer.data[(cursor + 1) * 6 + 5];
                }
            }
            else if(cursor == utf32_str.size()) {
                bottom_left = buffer.data[(cursor - 1) * 6 + 1];
                top_right = bottom_left;
                top_right[0] += 0.01;
            }
            else {
                bottom_left = buffer.data[cursor * 6];
                top_right = buffer.data[cursor * 6 + 2];
            }
            vec3 quad_pos = {bottom_left[0], 0.95f + buffer.vertical_extent[0], 0};
            vec3 quad_size = {
                top_right[0] - bottom_left[0],
                buffer.vertical_extent[1] - buffer.vertical_extent[0],
                0
            };
            //printf("cursor: %u (/%u)\n", cursor, utf32_str.size());
            //printf("quad_pos: "); print(quad_pos); printf("\n");
            //printf("quad_size: "); print(quad_size); printf("\n");
            mat4 button_m = translation_matrix(quad_pos) * scaling_matrix(quad_size);

            glUseProgram(quad_shader.id);
            glBindVertexArray(quad_vao);
            glUniform4f(glGetUniformLocation(quad_shader.id, "color"), 0, 0, 0,
                0.2 + 0.3 * sin(glfwGetTime()) * sin(glfwGetTime()));
            glUniformMatrix4fv(glGetUniformLocation(quad_shader.id, "m"), 1, GL_FALSE, (float*)&button_m);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        }

        glfwSwapBuffers(window);

        float tmp = glfwGetTime();
        dt = tmp - last_time;
        last_time = tmp;
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
