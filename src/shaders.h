#pragma once

#include <cstdio>
#include <string>
#include <fstream>

#include "load_gl.h"

struct Shader {
    uint32_t vert_id, frag_id;
    uint32_t id;
};

Shader make_shader_program(std::string name, std::string dir = "shaders");
Shader make_shader_program_src(const char* vert_src, const char* frag_src);
