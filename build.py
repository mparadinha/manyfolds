#!/usr/bin/env python3

import os
import sys
import shutil
import subprocess
import glob
import time
import json
import argparse


os.chdir(sys.path[0])
cur_dir = os.getcwd()


obj_dir = ".obj"
metadata_file = "metadata_file"
metadata_filepath = os.path.join(obj_dir, metadata_file)

executable = "run"

include_flags = "-I./include".split()
linker_flags = "-lglfw -ldl -lm".split()
compilation_flags = "-g -pedantic".split()
global_flags = "-fdiagnostics-color=always -std=c++2a".split()

source_files = glob.glob("**/*.cpp", recursive=True)

max_lines = 100

def print_msg(result):
    msg = result.stderr.decode("utf-8")
    lines = msg.splitlines()
    first_error_line = 0

    if result.returncode != 0:
        for i, line in enumerate(lines):
            if "error" in line:
                first_error_line = i
                break

    if len(lines) < max_lines or result.returncode == 0:
        print("\n".join(lines))
    else:
        warnings_line = min(max_lines // 2, first_error_line)
        print("\n".join(lines[:warnings_line]))

        if first_error_line > warnings_line + 1:
            print("\n...", first_error_line - warnings_line, "lines of output later ...\n")
        print("\n".join(lines[first_error_line : first_error_line + max_lines // 2]))

        if first_error_line + max_lines // 2 < len(lines):
            print("\n...", len(lines) - first_error_line + max_lines // 2, "lines ...")

def get_dependencies(src):
    # we need the color flag because subprocess is called via pipe by python and
    # gcc would not print any color codes for errors and warnings
    result = subprocess.run(["g++", src, "-MM"] + global_flags + include_flags, capture_output=True)
    if result.stderr: print_msg(result)
    if result.returncode != 0:
        return False

    output_str = result.stdout.decode("utf-8").replace("\\", "")
    gcc_deps = output_str.split(":")[-1].split()[1:]

    deps = set()
    for d in gcc_deps: deps.add(os.path.realpath(d))
    return list(deps)

def split_dir(filepath):
    return (os.path.dirname(filepath), os.path.basename(filepath))

def build_file(src):
    src_dir, src_file = split_dir(src.split(cur_dir)[-1])
    obj_file_dir = os.path.join(cur_dir, obj_dir, src_dir)
    obj_file = os.path.join(obj_file_dir, src_file.replace(".cpp", ".o"))
    # for source files that are under a directory we need to check, before calling gcc,
    # that we have the corresponding dir for obj files in obj_dir.
    # e.g.: system/file.cpp -> obj_dir/system/file.o
    if not os.path.exists(obj_file_dir): os.makedirs(obj_file_dir)

    result = subprocess.run(["g++", "-c", src, "-o", obj_file] + global_flags + compilation_flags + include_flags, capture_output=True)
    # warnings get sent to sterrr so we print it even if there was no error
    if result.stderr: print_msg(result)
    if result.returncode != 0:
        return False
    return obj_file

def is_dirty(src, metadata):
    if not src in metadata["file-times"]:
        metadata["file-times"][src] = 0
        metadata["sources"][src] = {"deps": list()}
        return True

    return os.path.getmtime(src) > metadata["file-times"][src]

def build(metadata):
    if not os.path.exists(obj_dir): os.makedirs(obj_dir)

    changed_anything = False

    # build all the object files
    obj_files = list()
    dirty_files = list()
    for src in source_files:
        should_build = False

        if is_dirty(src, metadata): # check if file has changed/is new
            should_build = True
            # update the metadata for this files dependencies
            deps = get_dependencies(src)
            for d in deps: metadata["file-times"][d] = os.path.getmtime(d)
            metadata["sources"][src] = { "deps": deps }


        else:
            # check if its the dependencies have changed
            for d in metadata["sources"][src]["deps"]:
                if is_dirty(d, metadata):
                    should_build = True
                    dirty_files.append(d) 
            
        if should_build:
            print("building", src)
            obj_file = build_file(src)
            if obj_file is False: return False # error with compilation
            else: obj_files.append(obj_file)

            dirty_files.append(src)

            changed_anything = True

        else: # we still need the all obj file paths in case we need to link
            obj_filepath = src.split(cur_dir)[-1].replace(".cpp", ".o")
            obj_files.append(os.path.join(cur_dir, obj_dir, obj_filepath))

    # now that we have built everything we need to we can update all the file times
    # (we only do this here and not before because multiple sources might depend on
    # the same file and we want them all to build)
    for dirty in dirty_files: metadata["file-times"][dirty] = os.path.getmtime(dirty)

    # TODO: this is only true if no libs were changed. if for some reason
    #   we update a library that we link with then we need to re-link even
    #   if our sources are unchanged

    # link
    if changed_anything or not os.path.exists(executable):
        print("linking...")
        result = subprocess.run(["g++", "-o", executable] + global_flags + obj_files + linker_flags, capture_output=True)
        if result.returncode != 0:
            print_msg(result)
            return False

    else:
        print("nothing to do.")

    return True

def main(args):
    if args.clean:
        if os.path.isfile(executable): os.remove(executable)
        if os.path.exists(obj_dir): shutil.rmtree(obj_dir)
        return
        
    if args.no_color: global_flags.remove("-fdiagnostics-color=always")

    # load metadata file
    if os.path.exists(metadata_filepath):
        with open(metadata_filepath, "r") as m_file:
            metadata = json.load(m_file)
    else:
        metadata = {"file-times": dict(), "sources": dict()}

    if not args.force is None:
        if args.force:
            for f in args.force:
                if f in metadata["file-times"]: metadata["file-times"][f] = 0

        else:
            metadata = {"file-times": dict(), "sources": dict()}

    success = build(metadata)

    # save the metadata back for the next run
    with open(metadata_filepath, "w") as m_file:
        json.dump(metadata, m_file, indent=2)

    if not success:
        print("build failed")
        sys.exit(1)

    # TODO: display the timing information
    # nothing for now


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--force", help="recompile specific files (everything if no file specified)",
        nargs="*")
    parser.add_argument("-c", "--clean", help="delete all build related files",
        action="store_true")
    parser.add_argument("--no-color", help="don't output color codes", action="store_true")
    args = parser.parse_args()

    main(args)
