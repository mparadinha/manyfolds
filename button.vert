#version 330 core

layout(location = 0) in vec2 pos;

uniform mat4 m;

void main() {
    gl_Position = m * vec4(pos, 0, 1);
}
